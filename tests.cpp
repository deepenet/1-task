#include "tritset.cpp"
#include <gtest/gtest.h>
#include <unordered_map>
 
TEST(capasity, owerflow) {
	tritset overflow(-1);
	EXPECT_EQ((uint(-1)*2 - 1)/(sizeof(uint)*8) + 1, overflow.capasity());
}

TEST(capasity, empty) {
	tritset empty;
	EXPECT_EQ(0, empty.capasity());
}

TEST(capasity, zero) {
	tritset zero(0);
	EXPECT_EQ(0, zero.capasity());
}

TEST(capasity, positive) {	
	tritset one(1);
	EXPECT_EQ(1, one.capasity());
	
	tritset fullUint(sizeof(uint)*4);
	EXPECT_EQ(1, fullUint.capasity());

	tritset fullUintPlusTrit(sizeof(uint)*4 + 1);
	EXPECT_EQ(2, fullUintPlusTrit.capasity());

	tritset usuallyTrit(sizeof(uint)*4*15 + 3);
	EXPECT_EQ(16, usuallyTrit.capasity());
}

TEST(numbTrits, overflow) {
	tritset overflow(-1);
	EXPECT_EQ(uint(-1), overflow.numbTrits());
}

TEST(numbTrits, empty) {
	tritset empty;
	EXPECT_EQ(0, empty.numbTrits());
}

TEST(numbTrits, zero) {
	tritset zero(0);
	EXPECT_EQ(0, zero.numbTrits());
}

TEST(numbTrits, positive) {	
	tritset one(1);
	EXPECT_EQ(1, one.numbTrits());
	
	tritset fullUint(sizeof(uint)*4);
	EXPECT_EQ(sizeof(uint)*4, fullUint.numbTrits());

	tritset fullUintPlusTrit(sizeof(uint)*4 + 1);
	EXPECT_EQ(sizeof(uint)*4 + 1, fullUintPlusTrit.numbTrits());

	tritset usuallyTrit(sizeof(uint)*4*15 + 3);
	EXPECT_EQ(sizeof(uint)*4*15 + 3, usuallyTrit.numbTrits());
}

TEST(squareBrackets, overBorder) {
	tritset brackets(1);

	EXPECT_EQ(Trit::Unknown, Trit(brackets[1]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[sizeof(uint)*4 - 1]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[256]));

	tritset empty;

	EXPECT_EQ(Trit::Unknown, Trit(empty[0]));
	EXPECT_EQ(Trit::Unknown, Trit(empty[1]));
	EXPECT_EQ(Trit::Unknown, Trit(empty[sizeof(uint)*4 - 1]));
	EXPECT_EQ(Trit::Unknown, Trit(empty[256]));
}

TEST(squareBrackets, onlyUnknown) {
	tritset brackets(128);

	EXPECT_EQ(Trit::Unknown, Trit(brackets[0]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[1]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[sizeof(uint)*4 - 1]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[127]));
}

TEST(squareBrackets, usually) {
	tritset brackets(sizeof(uint)*4*2);

	brackets[0] = Trit::False;
	brackets[1] = Trit::Unknown;
	brackets[2] = Trit::True;

	EXPECT_EQ(Trit::False, Trit(brackets[0]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[1]));
	EXPECT_EQ(Trit::True, Trit(brackets[2]));
}

TEST(squareBrackets, assignmentOverMainArray) {
	tritset brackets(sizeof(uint)*4*2);

	brackets[sizeof(uint)*4] = Trit::False;
	brackets[sizeof(uint)*4 + 1] = Trit::Unknown;
	brackets[sizeof(uint)*4 + 2] = Trit::True;

	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[sizeof(uint)*4 + 1]));
	EXPECT_EQ(Trit::True, Trit(brackets[sizeof(uint)*4 + 2]));
}

TEST(squareBrackets, sequentialAssignments) {
	tritset brackets(sizeof(uint)*4*3);

	brackets[0] = Trit::False;
	brackets[sizeof(uint)*4] = brackets[sizeof(uint)*4 + 1] = brackets[sizeof(uint)*4*2 + 1] = brackets[2] = brackets[1] = brackets[0];

	EXPECT_EQ(Trit::False, Trit(brackets[0]));
	EXPECT_EQ(Trit::False, Trit(brackets[1]));
	EXPECT_EQ(Trit::False, Trit(brackets[2]));
	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4*2 + 1]));
	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4 + 1]));
	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4]));
}

TEST(squareBrackets, bigSize) {
	tritset brackets(sizeof(uint)*4*250000);

	brackets[0] = Trit::False;
	brackets[1] = Trit::Unknown;
	brackets[2] = Trit::True;

	brackets[sizeof(uint)*4] = Trit::False;
	brackets[sizeof(uint)*4 + 1] = Trit::Unknown;
	brackets[sizeof(uint)*4 + 2] = Trit::True;

	brackets[sizeof(uint)*4*1000] = Trit::False;
	brackets[sizeof(uint)*4*1000 + 1] = Trit::Unknown;
	brackets[sizeof(uint)*4*1000 + 2] = Trit::True;

	brackets[sizeof(uint)*4*100000] = Trit::False;
	brackets[sizeof(uint)*4*100000 + 1] = Trit::Unknown;
	brackets[sizeof(uint)*4*100000 + 2] = Trit::True;

	brackets[sizeof(uint)*4*249995] = Trit::False;
	brackets[sizeof(uint)*4*249995 + 1] = Trit::Unknown;
	brackets[sizeof(uint)*4*249995 + 2] = Trit::True;

	EXPECT_EQ(Trit::False, Trit(brackets[0]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[1]));
	EXPECT_EQ(Trit::True, Trit(brackets[2]));

	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[sizeof(uint)*4 + 1]));
	EXPECT_EQ(Trit::True, Trit(brackets[sizeof(uint)*4 + 2]));

	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4*1000]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[sizeof(uint)*4*1000 + 1]));
	EXPECT_EQ(Trit::True, Trit(brackets[sizeof(uint)*4*1000 + 2]));

	EXPECT_EQ(Trit::False, Trit(brackets[sizeof(uint)*4*249995]));
	EXPECT_EQ(Trit::Unknown, Trit(brackets[sizeof(uint)*4*249995 + 1]));
	EXPECT_EQ(Trit::True, Trit(brackets[sizeof(uint)*4*249995 + 2]));
}

TEST(assignmentTritsets, oneUint) {
	tritset firstObject(4);
	firstObject[0] = Trit::False;
	firstObject[2] = Trit::True;
	tritset secondObject = firstObject;
	EXPECT_EQ(secondObject.numbTrits(), firstObject.numbTrits());
	for(int i = 0; i < 4; i++) {
		EXPECT_EQ(Trit(secondObject[i]), Trit(firstObject[i]));
	}
}

TEST(assignmentTritsets, empty) {
	tritset firstObject;
	tritset secondObject = firstObject;
	EXPECT_EQ(secondObject.numbTrits(), firstObject.numbTrits());
}

TEST(assignmentTritsets, aLotOfUints) {
	tritset firstObject(sizeof(uint)*4*5000);
	firstObject[0] = Trit::False;
	firstObject[2] = Trit::True;
	firstObject[sizeof(uint)*4 - 1] = Trit::False;
	firstObject[sizeof(uint)*4 - 1] = Trit::True;
	firstObject[sizeof(uint)*4*5 - 1] = Trit::False;
	firstObject[sizeof(uint)*4*5 + 1] = Trit::True;
	tritset secondObject = firstObject;
	EXPECT_EQ(secondObject.numbTrits(), firstObject.numbTrits());
	EXPECT_EQ(Trit(secondObject[0]), Trit(firstObject[0]));
	EXPECT_EQ(Trit(secondObject[2]), Trit(firstObject[2]));
	EXPECT_EQ(Trit(secondObject[sizeof(uint)*4 - 1]), Trit(firstObject[sizeof(uint)*4 - 1]));
	EXPECT_EQ(Trit(secondObject[sizeof(uint)*4 + 1]), Trit(firstObject[sizeof(uint)*4 + 1]));
	EXPECT_EQ(Trit(secondObject[sizeof(uint)*4*5 - 1]), Trit(firstObject[sizeof(uint)*4*5 - 1]));
	EXPECT_EQ(Trit(secondObject[sizeof(uint)*4*5 + 1]), Trit(firstObject[sizeof(uint)*4*5 + 1]));
	EXPECT_EQ(Trit(secondObject[sizeof(uint)*4*9 + 3]), Trit(firstObject[sizeof(uint)*4*5 + 3]));
}

TEST(assignmentTritsets, itself) {
	tritset object(sizeof(uint)*4*10);
	object[0] = Trit::False;
	object[2] = Trit::True;
	object[sizeof(uint)*4 - 1] = Trit::False;
	object[sizeof(uint)*4 - 1] = Trit::True;
	object[sizeof(uint)*4*5 - 1] = Trit::False;
	object[sizeof(uint)*4*5 + 1] = Trit::True;
	tritset objectCopy = object;
	EXPECT_EQ(object.numbTrits(), objectCopy.numbTrits());
	EXPECT_EQ(Trit(object[0]), Trit(objectCopy[0]));
	EXPECT_EQ(Trit(object[2]), Trit(objectCopy[2]));
	EXPECT_EQ(Trit(object[sizeof(uint)*4 - 1]), Trit(objectCopy[sizeof(uint)*4 - 1]));
	EXPECT_EQ(Trit(object[sizeof(uint)*4 + 1]), Trit(objectCopy[sizeof(uint)*4 + 1]));
	EXPECT_EQ(Trit(object[sizeof(uint)*4*5 - 1]), Trit(objectCopy[sizeof(uint)*4*5 - 1]));
	EXPECT_EQ(Trit(object[sizeof(uint)*4*5 + 1]), Trit(objectCopy[sizeof(uint)*4*5 + 1]));
	EXPECT_EQ(Trit(object[sizeof(uint)*4*9 + 3]), Trit(objectCopy[sizeof(uint)*4*5 + 3]));
}

TEST(shrink, empty) {
	tritset a;
	a.shrink();
	EXPECT_EQ(0, a.numbTrits());
}

TEST(shrink, fullUnknown) {
	tritset a(sizeof(uint)*4*2500);
	a.shrink();
	EXPECT_EQ(0, a.numbTrits());
}

TEST(shrink, lastTritNotUnknown) {
	tritset a(sizeof(uint)*4*2500);
	a[sizeof(uint)*4*2500 - 1] = Trit::True;
	a.shrink();
	EXPECT_EQ(sizeof(uint)*4*2500, a.numbTrits());
	a[sizeof(uint)*4*2500 - 1] = Trit::False;
	a.shrink();
	EXPECT_EQ(sizeof(uint)*4*2500, a.numbTrits());

	tritset b(sizeof(uint)*4*1234 + 2);
	b[sizeof(uint)*4*1234 + 1] = Trit::True;
	b.shrink();
	EXPECT_EQ(sizeof(uint)*4*1234 + 2, b.numbTrits());
	b[sizeof(uint)*4*1234 + 1] = Trit::False;
	b.shrink();
	EXPECT_EQ(sizeof(uint)*4*1234 + 2, b.numbTrits());
}

TEST(shrink, smallTail) {
	tritset a(sizeof(uint)*4*2500);
	a[sizeof(uint)*4*2500 - 3] = Trit::True;
	a.shrink();
	EXPECT_EQ(sizeof(uint)*4*2500 - 2, a.numbTrits());
}

TEST(shrink, longTail) {
	tritset a(sizeof(uint)*4*2500);
	a[sizeof(uint)*4*2500 - 3] = Trit::True;
	a.shrink();
	EXPECT_EQ(sizeof(uint)*4*2500 - 2, a.numbTrits());
}

TEST(logicAnd, empty) {
	tritset a;
	tritset b;
	tritset c = a & b;
	EXPECT_EQ(0, c.numbTrits());
}

TEST(logicAnd, truthTable) {
	tritset a(9);
	tritset b(9);
	a[0] = a[1] = a[2] = Trit::False;
	a[6] = a[7] = a[8] = Trit::True;
	b[0] = b[3] = b[6] = Trit::False;
	b[2] = b[5] = b[8] = Trit::True;
	tritset c = a & b;
	EXPECT_EQ(False, Trit(c[0]));
	EXPECT_EQ(False, Trit(c[1]));
	EXPECT_EQ(False, Trit(c[2]));
	EXPECT_EQ(False, Trit(c[3]));
	EXPECT_EQ(Unknown, Trit(c[4]));
	EXPECT_EQ(Unknown, Trit(c[5]));
	EXPECT_EQ(False, Trit(c[6]));
	EXPECT_EQ(Unknown, Trit(c[7]));
	EXPECT_EQ(True, Trit(c[8]));
}

TEST(logicAnd, longTritset) {
	tritset a(sizeof(uint)*4*900);
	tritset b(sizeof(uint)*4*900);
	a[0] = a[sizeof(uint)*4*100 - 1] = a[sizeof(uint)*4*200 - 1] = Trit::False;
	a[sizeof(uint)*4*600 - 1] = a[sizeof(uint)*4*700 - 1] = a[sizeof(uint)*4*800 - 1] = Trit::True;
	b[0] = b[sizeof(uint)*4*300 - 1] = b[sizeof(uint)*4*600 - 1] = Trit::False;
	b[sizeof(uint)*4*200 - 1] = b[sizeof(uint)*4*500 - 1] = b[sizeof(uint)*4*800 - 1] = Trit::True;
	tritset c = a & b;
	EXPECT_EQ(False, Trit(c[0]));
	EXPECT_EQ(False, Trit(c[sizeof(uint)*4*100 - 1]));
	EXPECT_EQ(False, Trit(c[sizeof(uint)*4*200 - 1]));
	EXPECT_EQ(False, Trit(c[sizeof(uint)*4*300 - 1]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*400 - 1]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*500 - 1]));
	EXPECT_EQ(False, Trit(c[sizeof(uint)*4*600 - 1]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*700 - 1]));
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*800 - 1]));
}

TEST(logicOr, empty) {
	tritset a;
	tritset b;
	tritset c = a | b;
	EXPECT_EQ(0, c.numbTrits());
}

TEST(logicOr, truthTable) {
	tritset a(9);
	tritset b(9);
	a[0] = a[1] = a[2] = Trit::False;
	a[3] = a[4] = a[5] = Trit::Unknown;
	a[6] = a[7] = a[8] = Trit::True;

	b[0] = b[3] = b[6] = Trit::False;
	b[1] = b[4] = b[7] = Trit::Unknown;
	b[2] = b[5] = b[8] = Trit::True;

	tritset c = a | b;

	EXPECT_EQ(False, Trit(c[0]));
	EXPECT_EQ(Unknown, Trit(c[1]));
	EXPECT_EQ(True, Trit(c[2]));
	EXPECT_EQ(Unknown, Trit(c[3]));
	EXPECT_EQ(Unknown, Trit(c[4]));
	EXPECT_EQ(True, Trit(c[5]));
	EXPECT_EQ(True, Trit(c[6]));
	EXPECT_EQ(True, Trit(c[7]));
	EXPECT_EQ(True, Trit(c[8]));
}

TEST(logicOr, longTritset) {
	tritset a(sizeof(uint)*4*900);
	tritset b(sizeof(uint)*4*900);
	a[0] = a[sizeof(uint)*4*100 - 1] = a[sizeof(uint)*4*200 - 1] = Trit::False;
	a[sizeof(uint)*4*600 - 1] = a[sizeof(uint)*4*700 - 1] = a[sizeof(uint)*4*800 - 1] = Trit::True;
	b[0] = b[sizeof(uint)*4*300 - 1] = b[sizeof(uint)*4*600 - 1] = Trit::False;
	b[sizeof(uint)*4*200 - 1] = b[sizeof(uint)*4*500 - 1] = b[sizeof(uint)*4*800 - 1] = Trit::True;
	tritset c = a | b;
	EXPECT_EQ(False, Trit(c[0]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*100 - 1]));
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*200 - 1]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*300 - 1]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*400 - 1]));
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*500 - 1]));
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*600 - 1]));
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*700 - 1]));
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*800 - 1]));
}

TEST(logicNot, empty) {
	tritset a;
	tritset c = ~a;
	EXPECT_EQ(0, c.numbTrits());
}

TEST(logicNot, truthTable) {
	tritset a(3);
	a[0] = Trit::False;
	a[1] = Trit::Unknown;
	a[2] = Trit::True;

	tritset c = ~a;

	EXPECT_EQ(True, Trit(c[0]));
	EXPECT_EQ(Unknown, Trit(c[1]));
	EXPECT_EQ(False, Trit(c[2]));
}

TEST(logicNot, longTritset) {
	tritset a(sizeof(uint)*4*900);
	a[sizeof(uint)*4*3 + 8] = Trit::False;
	a[sizeof(uint)*4*333 - 1] = Trit::Unknown;
	a[sizeof(uint)*4*666] = Trit::True;
	tritset c = ~a;
	EXPECT_EQ(True, Trit(c[sizeof(uint)*4*3 + 8]));
	EXPECT_EQ(Unknown, Trit(c[sizeof(uint)*4*333 - 1]));
	EXPECT_EQ(False, Trit(c[sizeof(uint)*4*666]));
}

TEST(cardinality, empty) {
	tritset a;
	EXPECT_EQ(0, a.cardinality(Trit::Unknown));
	EXPECT_EQ(0, a.cardinality(Trit::True));
	EXPECT_EQ(0, a.cardinality(Trit::False));
}

TEST(cardinality, allUnknown) {
	tritset a(sizeof(uint)*4*256);
	EXPECT_EQ(0, a.cardinality(Trit::Unknown));
	EXPECT_EQ(0, a.cardinality(Trit::True));
	EXPECT_EQ(0, a.cardinality(Trit::False));
}

TEST(cardinality, unknownTail) {
	tritset a(sizeof(uint)*4*64);
	a[0] = Trit::False;
	a[1] = Trit::Unknown;
	a[2] = Trit::True;
	a[sizeof(uint)*4] = Trit::False;
	a[sizeof(uint)*4 + 1] = Trit::Unknown;
	a[sizeof(uint)*4 + 2] = Trit::True;
	EXPECT_EQ(2, a.cardinality(Trit::False));
	EXPECT_EQ(sizeof(uint)*4 - 1, a.cardinality(Trit::Unknown));
	EXPECT_EQ(2, a.cardinality(Trit::True));
}

TEST(cardinalityAll, empty) {
	tritset a;
	std::unordered_map<Trit, size_t, std::hash<size_t>> map = a.cardinality();
	EXPECT_EQ(0, map.at(False));
	EXPECT_EQ(0, map.at(Unknown));
	EXPECT_EQ(0, map.at(True));
}

TEST(cardinalityAll, allUnknown) {
	tritset a(sizeof(uint)*4*256);
	std::unordered_map<Trit, size_t, std::hash<size_t>> map = a.cardinality();
	EXPECT_EQ(0, map.at(False));
	EXPECT_EQ(0, map.at(Unknown));
	EXPECT_EQ(0, map.at(True));
}

TEST(cardinalityAll, unknownTail) {
	tritset a(sizeof(uint)*4*64);
	a[0] = Trit::False;
	a[1] = Trit::Unknown;
	a[2] = Trit::True;
	a[sizeof(uint)*4] = Trit::False;
	a[sizeof(uint)*4 + 1] = Trit::Unknown;
	a[sizeof(uint)*4 + 2] = Trit::True;
	std::unordered_map<Trit, size_t, std::hash<size_t>> map = a.cardinality();
	EXPECT_EQ(2, map.at(False));
	EXPECT_EQ(sizeof(uint)*4 - 1, map.at(Unknown));
	EXPECT_EQ(2, map.at(True));

}


TEST(trim, empty) {
	tritset a;
	a.trim(5);
	EXPECT_EQ(0, a.numbTrits());
	a.trim(1);
	EXPECT_EQ(0, a.numbTrits());
	a.trim(0);
	EXPECT_EQ(0, a.numbTrits());
}

TEST(trim, overBorder) {
	tritset a(sizeof(uint)*4);
	a.trim(sizeof(uint)*4*2);
	EXPECT_EQ(sizeof(uint)*4, a.numbTrits());
}

TEST(trim, border) {
	tritset a(sizeof(uint)*4);
	a.trim(sizeof(uint)*4 - 1);
	EXPECT_EQ(sizeof(uint)*4 - 1, a.numbTrits());
}

TEST(trim, middle) {
	tritset a(sizeof(uint)*4*32);
	a.trim(sizeof(uint)*4*8 + 3);
	EXPECT_EQ(sizeof(uint)*4*8 + 3, a.numbTrits());
	a.trim(sizeof(uint)*4*8);
	EXPECT_EQ(sizeof(uint)*4*8, a.numbTrits());
	a.trim(sizeof(uint)*4*8 - 1);
	EXPECT_EQ(sizeof(uint)*4*8 - 1, a.numbTrits());
}

TEST(trim, savingValues) {
	tritset a(sizeof(uint)*4*33);
	for(int i = 0; i < sizeof(uint)*4*33; i += 3) {
		a[i] = False;
		a[i + 1] = Unknown;
		a[i + 2] = True;
	}
	a.trim(sizeof(uint)*4*9);
	for(int i = 0; i < sizeof(uint)*4*9; i += 3) {
		EXPECT_EQ(False, Trit(a[i]));
		EXPECT_EQ(Unknown, Trit(a[i + 1]));
		EXPECT_EQ(True, Trit(a[i + 2]));
	}
	EXPECT_EQ(Unknown, Trit(a[sizeof(uint)*4*9]));
	EXPECT_EQ(Unknown, Trit(a[sizeof(uint)*4*64 + 13]));
}

TEST(length, empty) {
	tritset a;
	EXPECT_EQ(0, a.length());
	tritset b(0);
	EXPECT_EQ(0, b.length());
}

TEST(length, fullUnknown) {
	tritset a(sizeof(uint)*4*128);
	EXPECT_EQ(0, a.length());
}

TEST(length, usually) {
	tritset a(sizeof(uint)*4*128);
	a[sizeof(uint)*4*128 - 5] = True;
	EXPECT_EQ(sizeof(uint)*4*128 - 4, a.length());
	a[sizeof(uint)*4*128 - 5] = Unknown;
	a[sizeof(uint)*4*125 - 5] = True;
	EXPECT_EQ(sizeof(uint)*4*125 - 4, a.length());
	a[sizeof(uint)*4*125 - 5] = Unknown;
	a[sizeof(uint)*4*98 - 1] = True;
	EXPECT_EQ(sizeof(uint)*4*98, a.length());
	a[sizeof(uint)*4*98 - 1] = Unknown;
	a[sizeof(uint)*4*95] = True;
	EXPECT_EQ(sizeof(uint)*4*95 + 1, a.length());
	a[sizeof(uint)*4*95] = Unknown;
	a[sizeof(uint)*4*95] = True;
	EXPECT_EQ(sizeof(uint)*4*95 + 1, a.length());
	a[sizeof(uint)*4*95] = Unknown;
	a[sizeof(uint)*4*94 + 1] = True;
	EXPECT_EQ(sizeof(uint)*4*94 + 2, a.length());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}