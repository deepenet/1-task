#include <iostream>
#include <unordered_map>

using uint = unsigned int;
//names of trits
enum Trit : uint{False = 0, Unknown, True};
class tritset
{
	//main array contains all trits
	uint *mainAry;
	//number of trits
	uint size;
	//proxy class for imitation one trit - 2 bits
	class trit
	{
		tritset *object;
		uint pos;
		bool overBorder;
	public:
		trit(tritset *objectRef, uint valPos, bool isOverBorder);
		trit& operator =(Trit second);
		trit& operator =(trit &secondTrit);
		operator Trit();
	};
	//increases main array size and copy all data
	void increaseMainAry(uint newSize);
	//calculates how much uints we need for input number of trits
	static size_t calcNumbUints(uint numbTrits);
	//creates uint variable which is filled by unknowns: 01010101...01 for example
	static uint makeUintFillUnknown();
public:
	tritset(uint numbTrits);
	tritset(const tritset &second);
	tritset();
	~tritset();
	//returns size of main array
	const size_t capasity() const;
	//returns number of trits - size
	const size_t numbTrits() const;
	//returns size of main array
	size_t capasity();
	//returns number of trits - size
	size_t numbTrits();
	//returns object of proxy class - imitation of one trit
	trit operator [](uint pos);
	//writes donw second tritset to first tritset and returns reference
	tritset& operator =(const tritset &second);
	//deletes insignificant unknowns trits
	void shrink();
	//returns new tritset - result of operator "and" for input tritsets
	tritset operator &(const tritset& second);
	//returns new tritset - result of operator "or" for input tritsets
	tritset operator |(const tritset& second);
	//returns new tritset - result of operator "not" for input tritset
	tritset operator ~();
	//returns number of trits set to this value; for unknowns - significant trits 
	size_t cardinality(Trit value);
	//returns number of trits for each values of trits;
	std::unordered_map<Trit, size_t, std::hash<size_t>> cardinality();
	//forget content form "lastIndex" and farther
	void trim(size_t lastIndex);
	//logical length - index last not Unknown trit +1
	size_t length();
};