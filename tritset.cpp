#include "tritset.h"
#include <iostream>

//using namespace Trits;

size_t tritset::calcNumbUints(uint numbTrits) {
	return (numbTrits - 1)/(4*sizeof(uint)) + 1;
}

uint tritset::makeUintFillUnknown() {
	uint uintFillUnknown = Unknown;
	for(int i = 1; i < sizeof(uint)*4; ++i) {
		uintFillUnknown <<= 2;
		uintFillUnknown += Unknown;
	}
	//may count this number before compile, put there - overloading helps us
	return uintFillUnknown;
}

tritset::trit::trit(tritset *objectRef, uint valPos, bool isOverBorder) {
	object = objectRef;
	pos = valPos;
	overBorder = isOverBorder;
}

tritset::trit& tritset::trit::operator =(Trit second) {
	if(overBorder && (second == Unknown)) {
		return *this;
	}
	if(overBorder) { 
		object->increaseMainAry(pos + 1);
		object->size = pos + 1;
		overBorder = 0;
	}
	uint mask = ~(3 << (sizeof(uint)*8 - ((pos%(sizeof(uint)*8))*2 + 2))); 	// byte: ...00011 => 00...001100...000 => 11...110011...11
	// byte: ...000AB => 00...00AB00...00 | xx...xx00xx...xx = xx...xxABxx...xx
	second = Trit(uint(second) << sizeof(uint)*8 - ((pos%(sizeof(uint)*8))*2 + 2)); 			
	object->mainAry[pos*2/(sizeof(uint)*8)] &= mask;
	object->mainAry[pos*2/(sizeof(uint)*8)] |= second;
	return *this;
}

tritset::trit& tritset::trit::operator =(tritset::trit &secondTrit) {
	uint second;
	if(secondTrit.overBorder) {
		second = Unknown;
	}
	else {
		uint mask = 3; // byte: ...xxxxAB & 00...0011 == 00...00AB
		second = (secondTrit.object->mainAry[secondTrit.pos*2/(sizeof(uint)*8)] >> (sizeof(uint)*8 - ((secondTrit.pos*2%(sizeof(uint)*8)) + 2))) & mask;
	}
	if(overBorder && second == Unknown) {
		return *this;
	}
	if(overBorder) { 
		if((calcNumbUints(pos) > calcNumbUints(object->size - 1))) { //if pos and last trit are in another uints
			object->increaseMainAry(pos + 1);
		}
		object->size = pos + 1;
		overBorder = 0;
	}
	uint mask = ~(3 << (sizeof(uint)*8 - ((pos%(sizeof(uint)*8))*2 + 2))); 	// byte: ...00011 => 00...001100...000 => 11...110011...11
	// byte: ...000AB => 00...00AB00...00 | xx...xx00xx...xx = xx...xxABxx...xx
	second <<= (sizeof(uint)*8 - ((pos%(sizeof(uint)*8))*2 + 2)); 			
	object->mainAry[pos*2/(sizeof(uint)*8)] &= mask;
	object->mainAry[pos*2/(sizeof(uint)*8)] |= second;
	return *this;
}

tritset::trit::operator Trit() {
	if(overBorder)
		return Unknown;
	uint mask = 3; // byte: ...xxxxAB & 00...0011 == 00...00AB
	return Trit((object->mainAry[pos*2/(sizeof(uint)*8)] >> (sizeof(uint)*8 - ((pos*2%(sizeof(uint)*8)) + 2))) & mask);
}

tritset::tritset(uint numbTrits) {
	size = numbTrits;
	if(numbTrits < 1) {
		mainAry = nullptr;
		return;
	}
	mainAry = new uint[calcNumbUints(numbTrits)];
	uint uintFillUnknown = makeUintFillUnknown();
	std::fill(mainAry, mainAry + (calcNumbUints(numbTrits)), uintFillUnknown);
}

tritset::tritset(const tritset &second) {
	if(!second.numbTrits()) {
		mainAry = nullptr;
	} else {
		mainAry = new uint[calcNumbUints(second.size)];
		for(int i = 0; i < calcNumbUints(second.size); ++i) {
			mainAry[i] = second.mainAry[i];
		}
	}
	size = second.size;
}
tritset::tritset() {
	size = 0;
	mainAry = nullptr;
}
tritset::~tritset() {
	delete[] mainAry;
	mainAry = nullptr;
}
void tritset::increaseMainAry(uint newSize) {
	uint numbUints = calcNumbUints(size);
	uint numbNewUints = calcNumbUints(newSize);
	if(numbUints == numbNewUints)
		return;
	uint uintFillUnknown = makeUintFillUnknown();
	uint *newAry = new uint[numbNewUints];
	for(uint i = 0; i < numbUints; ++i) {
		newAry[i] = mainAry[i];
	}
	for(uint i = numbUints; i < numbNewUints; ++i) {
		newAry[i] = uintFillUnknown;
	}
	delete[] mainAry;
	mainAry = newAry;
	newAry = nullptr;
	size = newSize;
}
const size_t tritset::capasity() const {
	if(size < 1)
		return 0;
	return (size*2 - 1)/(sizeof(uint)*8) + 1;
}
const size_t tritset::numbTrits() const {
	return size;
}
size_t tritset::capasity() {
	if(size < 1)
		return 0;
	return (size*2 - 1)/(sizeof(uint)*8) + 1;
}
size_t tritset::numbTrits() {
	return size;
}
tritset::trit tritset::operator [](uint pos) {
	return trit(this, pos, pos >= size);
}
tritset& tritset::operator =(const tritset &second) {
	if(this->mainAry == second.mainAry)
		return *this;
	uint *newAry = new uint[calcNumbUints(second.size)];
	for(int i = 0; i < calcNumbUints(second.size); ++i) {
		newAry[i] = second.mainAry[i];
	}
	delete[] mainAry;
	mainAry = newAry;
	size = second.size;
	return *this;
}
void tritset::shrink() {
	if(!size)
		return;
	uint uintFillUnknown = makeUintFillUnknown();
	uint lastUint = calcNumbUints(size) - 1;
	uint rudiment = 0;
	uint firstTritNull = ~(3 << (sizeof(uint)*8 - 2)); // 00111...11
	uint mask = 3;
	//delete uints which are filled by unknown
	for(int i = lastUint; i >= 0; --i) {
		if(mainAry[i] == uintFillUnknown) {
			mainAry[i] = 0;
			rudiment++;
		} else {
			break;
		}
	}
	if(lastUint >= rudiment) {
		//subtraction deleted trits from size 18 -= 16 - (16 - 2)
		if(size%(sizeof(uint)*4))
			size -= rudiment*sizeof(uint)*4 - (sizeof(uint)*4 - size%(sizeof(uint)*4));
		uint lastSignificantUint = mainAry[lastUint - rudiment];
		//count trits which are filled by unknown
		for(int i = 0; i < sizeof(uint)*4; i++) {
			if((lastSignificantUint & mask) == Unknown) {
				lastSignificantUint >>= 2;
				lastSignificantUint &= firstTritNull;
				size--;
			} else {
				break;
			}
		}
		//rebuil main array
		uint *newAry = new uint[lastUint - rudiment + 1];
		for(int i = 0; i <= lastUint - rudiment; ++i) {
			newAry[i] = mainAry[i];
		}
		delete[] mainAry;
		mainAry = newAry;
		newAry = nullptr;
	} else {
		delete[] mainAry;
		mainAry = nullptr;
		size = 0;
	}
}
tritset tritset::operator &(const tritset& second) {
	uint answSize = this->size > second.size ? this->size : second.size;
	uint minSize = this->size < second.size ? this->size : second.size;
	tritset out(answSize);
	if(!answSize) {
		out.mainAry = nullptr;
		return out;
	}
	for(uint i = 0; i < calcNumbUints(minSize); i++) {
		out.mainAry[i] = 0;
		uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
		for(int j = 0; j < sizeof(uint)*4; j++) {
			out.mainAry[i] |= (((this->mainAry[i] & mask) < (second.mainAry[i] & mask)) ? (this->mainAry[i] & mask) : (second.mainAry[i] & mask));
			mask >>= 2;
			mask &= ~(3 << (sizeof(uint)*8 - 2));
		}
	}
	if(minSize == this->size) {
		for(uint i = calcNumbUints(minSize); i < calcNumbUints(answSize); i++) {
			out.mainAry[i] = 0;
			uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
			for(int j = 0; j < sizeof(uint)*4; j++) {
				out.mainAry[i] |= (((Unknown << (sizeof(uint)*8 - j*2 - 2)) < (second.mainAry[i] & mask)) ? (Unknown << (sizeof(uint)*8 - j*2 - 2)) : (second.mainAry[i] & mask));
				mask >>= 2;
				mask &= ~(3 << (sizeof(uint)*8 - 2));
			}
		}
	} else {
		for(uint i = calcNumbUints(minSize); i < calcNumbUints(answSize); i++) {
			out.mainAry[i] = 0;
			uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
			for(int j = 0; j < sizeof(uint)*4; j++) {
				out.mainAry[i] |= (((this->mainAry[i] & mask) < (Unknown << (sizeof(uint)*8 - j*2 - 2))) ? (this->mainAry[i] & mask) : (Unknown << (sizeof(uint)*8 - j*2 - 2)));
				mask >>= 2;
				mask &= ~(3 << (sizeof(uint)*8 - 2));
			}
		}
	}
	return out;
}
tritset tritset::operator |(const tritset& second) {
	uint answSize = this->size > second.size ? this->size : second.size;
	uint minSize = this->size < second.size ? this->size : second.size;
	tritset out(answSize);
	if(!answSize) {
		out.mainAry = nullptr;
		return out;
	}
	for(uint i = 0; i < calcNumbUints(minSize); i++) {
		out.mainAry[i] = 0;
		uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
		for(int j = 0; j < sizeof(uint)*4; j++) {
			out.mainAry[i] |= (((this->mainAry[i] & mask) > (second.mainAry[i] & mask)) ? (this->mainAry[i] & mask) : (second.mainAry[i] & mask));
			mask >>= 2;
			mask &= ~(3 << (sizeof(uint)*8 - 2));
		}
	}
	if(minSize == this->size) {
		for(uint i = calcNumbUints(minSize); i < calcNumbUints(answSize); i++) {
			out.mainAry[i] = 0;
			uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
			for(int j = 0; j < sizeof(uint)*4; j++) {
				out.mainAry[i] |= (((Unknown << (sizeof(uint)*8 - j*2 - 2)) > (second.mainAry[i] & mask)) ? (Unknown << (sizeof(uint)*8 - j*2 - 2)) : (second.mainAry[i] & mask));
				mask >>= 2;
				mask &= ~(3 << (sizeof(uint)*8 - 2));
			}
		}
	} else {
		for(uint i = calcNumbUints(minSize); i < calcNumbUints(answSize); i++) {
			out.mainAry[i] = 0;
			uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
			for(int j = 0; j < sizeof(uint)*4; j++) {
				out.mainAry[i] |= (((this->mainAry[i] & mask) > (Unknown << (sizeof(uint)*8 - j*2 - 2))) ? (this->mainAry[i] & mask) : (Unknown << (sizeof(uint)*8 - j*2 - 2)));
				mask >>= 2;
				mask &= ~(3 << (sizeof(uint)*8 - 2));
			}
		}
	}
	return out;
}
tritset tritset::operator ~() {
	uint answSize = this->size;
	tritset out(answSize);
	if(!answSize) {
		out.mainAry = nullptr;
		return out;
	}
	for(uint i = 0; i < calcNumbUints(answSize); i++) {
		out.mainAry[i] = 0;
		uint mask = 3 << (sizeof(uint)*8 - 2); // 11000..00
		for(int j = 0; j < sizeof(uint)*4; j++) {
			out.mainAry[i] |= ((2 - ((this->mainAry[i] & mask) >> (sizeof(uint)*8 - j*2 - 2))) << (sizeof(uint)*8 - j*2 - 2));
			mask >>= 2;
			mask &= ~(3 << (sizeof(uint)*8 - 2));
		}
	}
	return out;
}

size_t tritset::cardinality(Trit value) {
	size_t counter = 0;
	uint mask = 3; // byte: ...xxxxAB & 00...0011 == 00...00AB
	bool significant = false;
	if(value != Unknown)
		significant = true;
	for(int pos = size - 1; pos >= 0; pos--) {
		if(significant && ((mainAry[pos*2/(sizeof(uint)*8)] >> (sizeof(uint)*8 - ((pos*2%(sizeof(uint)*8)) + 2))) & mask) == value)
			counter++;
		else if(((mainAry[pos*2/(sizeof(uint)*8)] >> (sizeof(uint)*8 - ((pos*2%(sizeof(uint)*8)) + 2))) & mask) != Unknown)
			significant = true;
	}
	return counter;
}

std::unordered_map<Trit, size_t, std::hash<size_t>> tritset::cardinality() {
	std::unordered_map<Trit, size_t, std::hash<size_t>> out;
	out.emplace(False, 0);
	out.emplace(Unknown, 0);
	out.emplace(True, 0);
	auto falsePtr = out.find(False);
	auto unknownPtr = out.find(Unknown);
	auto truePtr = out.find(True);
	Trit tmp;
	uint mask = 3; // byte: ...xxxxAB & 00...0011 == 00...00AB
	bool significant = false;
	for(int pos = size - 1; pos >= 0; pos--) {
		tmp = Trit((mainAry[pos*2/(sizeof(uint)*8)] >> (sizeof(uint)*8 - ((pos*2%(sizeof(uint)*8)) + 2))) & mask);
		if(tmp != Unknown) {
			significant = true;
		}
		if(significant) {
			switch(tmp) {
				case False: out.at(False)++; break;
				case Unknown: out.at(Unknown)++; break;
				case True: out.at(True)++;
			}
		}
	}
	return out;
}

void tritset::trim(size_t lastIndex) {
	if(lastIndex >= size) {
		return;
	}
	uint uintFillUnknown = makeUintFillUnknown();
	uint lastUint = calcNumbUints(size) - 1;
	uint lastUintInclLastIndex = calcNumbUints(lastIndex) - 1;
	uint rudiment = 0;
	uint firstTritNull = ~(3 << (sizeof(uint)*8 - 2)); // 00111...11
	uint mask = 3;
	//subtraction deleted trits from size
	size = lastIndex;
	//fill farther trits by unknown
	int border = lastIndex - lastUintInclLastIndex*sizeof(uint)*4;
	int maskUnknown = 1; 	//00...01
	for(int i = sizeof(uint)*4 - 1; i < border; i++) {
		maskUnknown << 2; 	//00..0100
		maskUnknown++; 		//00..0101
	}
	int maskEmpty = ((~(0)) << border*2); // 11...1100..00
	mainAry[lastUintInclLastIndex] &= maskEmpty; 	//xx..xx00..00
	mainAry[lastUintInclLastIndex] |= maskUnknown;	//xx..xx01..01
	//rebuil main array
	uint *newAry = new uint[lastUintInclLastIndex + 1];
	for(int i = 0; i <= lastUintInclLastIndex; ++i) {
		newAry[i] = mainAry[i];
	}
	delete[] mainAry;
	mainAry = newAry;
	newAry = nullptr;
}

size_t tritset::length() {
	if(!size)
		return 0;
	uint uintFillUnknown = makeUintFillUnknown();
	uint lastUint = calcNumbUints(size) - 1;
	uint rudiment = 0;
	uint firstTritNull = ~(3 << (sizeof(uint)*8 - 2)); // 00111...11
	uint mask = 3;
	//skip uints which are filled by unknown
	for(int i = lastUint; i >= 0; --i) {
		if(mainAry[i] == uintFillUnknown) {
			rudiment++;
		} else {
			break;
		}
	}
	size_t counter = 0;
	if(lastUint >= rudiment) {
		uint lastSignificantUint = mainAry[lastUint - rudiment];
		//count trits which are filled by unknown
		for(int i = 0; i < sizeof(uint)*4; i++) {
			if((lastSignificantUint & mask) == Unknown) {
				lastSignificantUint >>= 2;
				lastSignificantUint &= firstTritNull;
				counter++;
			} else {
				break;
			}
		}
		return (lastUint - rudiment + 1)*sizeof(uint)*4 - counter;
	} else {
		return 0;
	}
}