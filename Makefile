CC = g++

CFlags = -O3 -std=c++17

all: build

build: main.o tritset.o
	$(CC) main.o tritset.o -o main

main.o: main.cpp
	$(CC) $(CFlags) main.cpp -c

tritset.o: tritset.cpp tritset.h
	$(CC) $(CFlags) tritset.cpp -c

clear:
	rm *.o